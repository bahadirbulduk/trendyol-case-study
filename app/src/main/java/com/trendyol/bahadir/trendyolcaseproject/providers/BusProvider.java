package com.trendyol.bahadir.trendyolcaseproject.providers;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

/**
 * Created by Bahadir on 25.02.2017.
 */

public final class BusProvider {

    private static final MainThreadBus defaultBus = new MainThreadBus();
    private static final MainThreadBus persistBus = new MainThreadBus();

    private BusProvider() {
    }

    public static MainThreadBus getInstance() {
        return defaultBus;
    }

    public static MainThreadBus getPersistInstance() {
        return persistBus;
    }

    public static class MainThreadBus extends Bus {
        private final Handler handler = new Handler(Looper.getMainLooper());

        @Override
        public void post(final Object event) {
            if (Looper.myLooper() == Looper.getMainLooper()) {
                super.post(event);
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        MainThreadBus.super.post(event);
                    }
                });
            }
        }
    }

}