package com.trendyol.bahadir.trendyolcaseproject.enums;

/**
 * Created by Bahadir on 25.02.2017.
 */

public enum EventErrorType {
    EXCEPTION, RETROFIT_ERROR, MESSAGE
}