package com.trendyol.bahadir.trendyolcaseproject.utils;


import android.support.v4.app.Fragment;

import com.trendyol.bahadir.trendyolcaseproject.events.ChangeFragment;
import com.trendyol.bahadir.trendyolcaseproject.providers.BusProvider;

public final class BusUtil {

    private BusUtil() {
    }

    public static void changeFragment(Fragment fragment, boolean addToStack) {
        BusProvider.getInstance().post(new ChangeFragment(fragment, addToStack));
    }

    public static void changeFragment(Fragment fragment) {
        BusProvider.getInstance().post(new ChangeFragment(fragment));
    }

}