package com.trendyol.bahadir.trendyolcaseproject.services.response;

import com.google.gson.annotations.SerializedName;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.CommentWrapper;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class CommentResponse {
    @SerializedName("comments")
    private CommentWrapper wrapper;

    public CommentWrapper getWrapper() {
        return wrapper;
    }
}
