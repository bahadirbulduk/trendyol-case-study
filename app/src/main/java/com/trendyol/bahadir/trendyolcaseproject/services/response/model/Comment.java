package com.trendyol.bahadir.trendyolcaseproject.services.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class Comment {

    @SerializedName("id")
    private String id;
    @SerializedName("author")
    private String author;
    @SerializedName("author_is_deleted")
    private String authorIsDeleted;
    @SerializedName("authorname")
    private String authorName;
    @SerializedName("iconserver")
    private String iconServer;
    @SerializedName("iconfarm")
    private String iconFarm;
    @SerializedName("datecreate")
    private String dateCreate;
    @SerializedName("permalink")
    private String permalink;
    @SerializedName("path_alias")
    private String pathAlias;
    @SerializedName("realname")
    private String realName;
    @SerializedName("_content")
    private String content;

    public String getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public String getAuthorIsDeleted() {
        return authorIsDeleted;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getIconServer() {
        return iconServer;
    }

    public String getIconFarm() {
        return iconFarm;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public String getPermalink() {
        return permalink;
    }

    public String getPathAlias() {
        return pathAlias;
    }

    public String getRealName() {
        return realName;
    }

    public String getContent() {
        return content;
    }
}
