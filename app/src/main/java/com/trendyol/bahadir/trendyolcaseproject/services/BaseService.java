package com.trendyol.bahadir.trendyolcaseproject.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.trendyol.bahadir.trendyolcaseproject.BuildConfig;

import java.util.concurrent.TimeUnit;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class BaseService {

    private BaseService() {
    }

    private static RestAdapter.Builder getBuilder() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(BuildConfig.CONNECTION_TIMEOUT,
                TimeUnit.MILLISECONDS);
        client.setReadTimeout(BuildConfig.CONNECTION_TIMEOUT,
                TimeUnit.MILLISECONDS);
        client.setWriteTimeout(BuildConfig.CONNECTION_TIMEOUT,
                TimeUnit.MILLISECONDS);
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addQueryParam("api_key", "fec54d493aeef0cffdc6192a6a954aac");
                request.addQueryParam("format", "json");
                request.addQueryParam("nojsoncallback", "1");
            }
        };

        return new RestAdapter.Builder()
                .setEndpoint("http://www.flickr.com/services")
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(client))
                .setErrorHandler(new ApiErrorHandler());
    }

    public static RestInterface getApi() {
        Gson gson = new GsonBuilder().create();

        return getBuilder().setConverter(new GsonConverter(gson))
                .build().create(RestInterface.class);
    }
}
