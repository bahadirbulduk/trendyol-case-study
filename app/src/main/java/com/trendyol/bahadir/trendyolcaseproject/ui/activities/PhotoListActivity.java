package com.trendyol.bahadir.trendyolcaseproject.ui.activities;

import com.trendyol.bahadir.trendyolcaseproject.R;
import com.trendyol.bahadir.trendyolcaseproject.ui.fragments.BaseFragment;
import com.trendyol.bahadir.trendyolcaseproject.ui.fragments.PhotoListFragment;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class PhotoListActivity extends BaseActivity {
    @Override
    protected BaseFragment getFragment() {
        return PhotoListFragment.newInstance();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_base_template;
    }
}
