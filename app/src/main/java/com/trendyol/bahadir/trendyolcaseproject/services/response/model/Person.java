package com.trendyol.bahadir.trendyolcaseproject.services.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class Person {

    /**
     * I didn't parse all response. Just necassary parameters parsed!!
     */
    @SerializedName("id")
    private String id;
    @SerializedName("nsid")
    private String nsid;
    @SerializedName("ispro")
    private Integer ispro;
    @SerializedName("can_buy_pro")
    private Integer can_buy_pro;
    @SerializedName("iconserver")
    private String iconserver;
    @SerializedName("iconfarm")
    private Integer iconfarm;
    @SerializedName("path_alias")
    private String path_alias;
    @SerializedName("has_stats")
    private String has_stats;
    @SerializedName("username")
    private PersonUserName personUserName;

    public String getId() {
        return id;
    }

    public String getNsid() {
        return nsid;
    }

    public Integer getIspro() {
        return ispro;
    }

    public Integer getCan_buy_pro() {
        return can_buy_pro;
    }

    public String getIconserver() {
        return iconserver;
    }

    public Integer getIconfarm() {
        return iconfarm;
    }

    public String getPath_alias() {
        return path_alias;
    }

    public String getHas_stats() {
        return has_stats;
    }

    public PersonUserName getPersonUserName() {
        return personUserName;
    }
}
