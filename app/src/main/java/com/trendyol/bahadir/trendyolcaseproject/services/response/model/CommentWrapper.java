package com.trendyol.bahadir.trendyolcaseproject.services.response.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class CommentWrapper {

    @SerializedName("photo_id")
    private String photoId;
    @SerializedName("comment")
    private ArrayList<Comment> comments;

    public String getPhotoId() {
        return photoId;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }
}
