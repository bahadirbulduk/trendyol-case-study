package com.trendyol.bahadir.trendyolcaseproject.services.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class PersonUserName {

    @SerializedName("_content")
    private String userName;

    public String getUserName() {
        return userName;
    }
}
