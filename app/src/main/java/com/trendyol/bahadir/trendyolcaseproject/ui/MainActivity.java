package com.trendyol.bahadir.trendyolcaseproject.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.trendyol.bahadir.trendyolcaseproject.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
