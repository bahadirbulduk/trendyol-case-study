package com.trendyol.bahadir.trendyolcaseproject.providers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public final class PreferenceProvider {

  private PreferenceProvider() {
  }

  public static int getInt(Context context, final String key, int defaultValue) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    return prefs.getInt(key, defaultValue);
  }

  public static void putInt(Context context, final String key, int value) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    prefs.edit().putInt(key, value).apply();
  }

  public static String getString(Context context, final String key, final String defaultValue) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    return prefs.getString(key, defaultValue);
  }

  public static void putString(Context context, final String key, final String value) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    prefs.edit().putString(key, value).apply();
  }

  public static boolean getBoolean(Context context, final String key, final boolean defaultValue) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    return prefs.getBoolean(key, defaultValue);
  }

  public static void putBoolean(Context context, final String key, final boolean value) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    prefs.edit().putBoolean(key, value).apply();
  }

  public static void remove(Context context, final String key) {
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
    prefs.edit().remove(key).apply();
  }
}
