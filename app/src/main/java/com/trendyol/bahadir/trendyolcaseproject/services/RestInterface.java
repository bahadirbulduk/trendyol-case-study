package com.trendyol.bahadir.trendyolcaseproject.services;

import com.trendyol.bahadir.trendyolcaseproject.services.response.CommentResponse;
import com.trendyol.bahadir.trendyolcaseproject.services.response.RecentPhotosResponse;
import com.trendyol.bahadir.trendyolcaseproject.services.response.UserInfoResponse;


import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;


public interface RestInterface {

    @GET("/rest")
    void getRecentPhotos(@Query("method") String method,
                         @Query("page") int page,
                         @Query("per_page") int perPage,
                         Callback<RecentPhotosResponse> response);

    @GET("/rest?tags=trendyol")
    void getTrendyolPhotos(@Query("method") String method,
                           @Query("page") int page,
                           Callback<RecentPhotosResponse> response);

    @GET("/rest?method=flickr.people.getInfo")
    void getUserInfo(@Query("user_id") String userId, Callback<UserInfoResponse> callback);

    @GET("/rest?method=flickr.photos.comments.getList")
    void getPhotoComment(@Query("photo_id") String photoId, Callback<CommentResponse> callback);
}
