package com.trendyol.bahadir.trendyolcaseproject.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.squareup.otto.Subscribe;
import com.trendyol.bahadir.trendyolcaseproject.R;
import com.trendyol.bahadir.trendyolcaseproject.events.ChangeFragment;
import com.trendyol.bahadir.trendyolcaseproject.providers.BusProvider;
import com.trendyol.bahadir.trendyolcaseproject.ui.fragments.BaseFragment;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

  private Object baseBusEventListener;

  @Override
  protected void onCreate(Bundle bundle) {
    super.onCreate(bundle);
    setContentView(getLayoutResId());
    ButterKnife.bind(this);
    baseBusEventListener = new Object() {

      @Subscribe
      public void changeFragment(ChangeFragment event) {
        setFragment(event.fragment, event.addToBackStack);
      }
    };
    onNewIntent(getIntent());
  }

  @Override
  protected void onNewIntent(Intent intent) {
    setFragment(getFragment(), false);

  }

  @Override
  protected void onStart() {
    super.onStart();
    BusProvider.getInstance().register(baseBusEventListener);
  }

  @Override
  protected void onStop() {
    try {
      BusProvider.getInstance().unregister(baseBusEventListener);
    } catch (Exception ignore) {
    }
    super.onStop();
  }


  protected void setFragment(@IdRes int layoutId, Fragment fragment, boolean addToBackStack) {
    FragmentManager fragmentManager = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    fragmentTransaction.replace(layoutId, fragment, null);
    if (addToBackStack) {
      fragmentTransaction.addToBackStack(null);
    }
    try {
      fragmentTransaction.commit();
    } catch (Exception e) {
      Log.e(e.getMessage(), e.getMessage());
      try {
        fragmentTransaction.commitAllowingStateLoss();
      } catch (Exception ex) {
        Log.e(e.getMessage(), e.getMessage());
      }
    }
  }

  protected void setFragment(Fragment fragment, boolean addToBackStack) {
    setFragment(R.id.frame_layout, fragment, addToBackStack);
  }

  protected abstract BaseFragment getFragment();

  protected abstract int getLayoutResId();

  @Override
  public void onBackPressed() {
    Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_layout);
    if (currentFragment != null && currentFragment instanceof BaseFragment) {
      BaseFragment frag = (BaseFragment) currentFragment;
      if (frag.onBackPressed()) {
        return;
      }
    }
    super.onBackPressed();
  }
}