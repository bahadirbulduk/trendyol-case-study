package com.trendyol.bahadir.trendyolcaseproject.events;

import android.support.v4.app.Fragment;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class ChangeFragment extends BaseEvent {
    public Fragment fragment;
    public boolean addToBackStack;

    public ChangeFragment(Fragment fragment) {
        this.fragment = fragment;
        this.addToBackStack = true;
    }

    public ChangeFragment(Fragment fragment, boolean addToBackStack) {
        this.fragment = fragment;
        this.addToBackStack = addToBackStack;
    }

}