package com.trendyol.bahadir.trendyolcaseproject.events;

import com.trendyol.bahadir.trendyolcaseproject.enums.EventErrorType;

import retrofit.RetrofitError;

/**
 * Created by Bahadir on 25.02.2017.
 */


public class BaseEvent {
    public Object error;
    public EventErrorType errorType;

    public BaseEvent() {
    }

    public BaseEvent(RetrofitError e) {
        this.error = e;
        this.errorType = EventErrorType.RETROFIT_ERROR;
    }

    public BaseEvent(Exception e) {
        this.error = e;
        this.errorType = EventErrorType.EXCEPTION;
    }

    public BaseEvent(String e, EventErrorType type) {
        this.error = e;
        this.errorType = EventErrorType.MESSAGE;
    }

    public boolean hasError() {
        return error != null;
    }

    public RetrofitError getRetrofitError() {
        if (error != null && error instanceof RetrofitError) {
            return (RetrofitError) error;
        }
        return null;
    }

    public Exception getError() {
        if (error != null && error instanceof Exception) {
            return (Exception) error;
        }
        return null;
    }

    public String getErrorMessage() {
        switch (errorType) {
            case EXCEPTION:
                return ((Exception) error).getMessage();
            case RETROFIT_ERROR:
                return ((RetrofitError) error).getMessage();
            case MESSAGE:
                return (String) error;
            default:
                return null;
        }
    }
}