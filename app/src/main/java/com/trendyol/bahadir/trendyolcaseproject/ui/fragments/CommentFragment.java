package com.trendyol.bahadir.trendyolcaseproject.ui.fragments;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.trendyol.bahadir.trendyolcaseproject.R;
import com.trendyol.bahadir.trendyolcaseproject.services.PhotoService;
import com.trendyol.bahadir.trendyolcaseproject.services.response.CommentResponse;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.Comment;
import com.trendyol.bahadir.trendyolcaseproject.ui.customviews.CircleImageView;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class CommentFragment extends BaseFragment {

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.comment_rows)
    protected LinearLayout commentRows;
    @BindView(R.id.no_comment)
    protected TextView noComment;

    private ArrayList<Comment> commentArrayList = new ArrayList<>();

    public static CommentFragment newInstance(String imageId) {
        CommentFragment f = new CommentFragment();
        Bundle b = new Bundle();
        b.putString("image_id", imageId);
        f.setArguments(b);
        return f;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_comment;
    }

    @Override
    protected void init() {

        toolbar.setTitle("Yorumlar");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String imageId = getArguments().getString("image_id");
        PhotoService.getPhotoComments(imageId, new Callback<CommentResponse>() {
            @Override
            public void success(CommentResponse commentResponse, Response response) {
                if (commentResponse.getWrapper().getComments() == null) {
                    noComment.setVisibility(View.VISIBLE);
                } else {
                    noComment.setVisibility(View.GONE);
                    commentArrayList.addAll(commentResponse.getWrapper().getComments());
                    addCommentViewRows();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                noComment.setVisibility(View.VISIBLE);
            }
        });
    }

    private void addCommentViewRows() {
        commentRows.removeAllViews();
        for (int i = 0; i < commentArrayList.size(); i++) {
            //comment.getEnteredDate()
            Comment comment = commentArrayList.get(i);
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.view_comment_item, commentRows, false);
            final de.hdodenhof.circleimageview.CircleImageView userImage = (de.hdodenhof.circleimageview.CircleImageView) v.findViewById(R.id.comment_user_image);
            final TextView userName = (TextView) v.findViewById(R.id.comment_user_name);
            TextView textViewComment = (TextView) v.findViewById(R.id.comment);

            userName.setText(comment.getRealName());

            textViewComment.setText(comment.getContent());

            String userImageUrl;
            if (comment.getIconServer().equals("0")) {
                userImageUrl = "https://www.flickr.com/images/buddyicon.png";
            } else {
                userImageUrl = "http://farm" + comment.getIconFarm() + ".staticflickr.com/" + comment.getIconServer() + "/buddyicons/" + comment.getAuthor() + ".jpg";
            }
            Glide.with(getActivity())
                    .load(userImageUrl)
                    .crossFade()
                    .fitCenter()
                    .into(userImage);

            commentRows.addView(v, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        }
    }

    @Override
    protected void onSaveState(Bundle bundle) {

    }

    @Override
    protected void onRestoreState(Bundle bundle) {

    }

    @Override
    protected void onRestore() {

    }

}
