package com.trendyol.bahadir.trendyolcaseproject.services;

import android.telecom.Call;

import com.trendyol.bahadir.trendyolcaseproject.services.response.CommentResponse;
import com.trendyol.bahadir.trendyolcaseproject.services.response.RecentPhotosResponse;
import com.trendyol.bahadir.trendyolcaseproject.ui.activities.BaseActivity;

import retrofit.Callback;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class PhotoService {

    public static void getRecentlyAddedPhotos(int page, int perPage, Callback<RecentPhotosResponse> callback) {
        BaseService.getApi().getRecentPhotos("flickr.photos.getRecent", page, perPage, callback);
    }

    public static void getTrendyolPhotos(int page, Callback<RecentPhotosResponse> callback) {
        BaseService.getApi().getTrendyolPhotos("flickr.photos.search", page, callback);
    }

    public static void getPhotoComments(String photoId, Callback<CommentResponse> callback) {
        BaseService.getApi().getPhotoComment(photoId, callback);
    }
}
