package com.trendyol.bahadir.trendyolcaseproject.services.response;

import com.google.gson.annotations.SerializedName;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.PhotoList;

public class RecentPhotosResponse {

    @SerializedName("photos")
    private PhotoList photos;
    @SerializedName("stat")
    private String stat;

    public PhotoList getPhotos() {
        return photos;
    }

    public String getStat() {
        return stat;
    }
}
