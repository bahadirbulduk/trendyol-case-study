package com.trendyol.bahadir.trendyolcaseproject.ui.fragments;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import com.trendyol.bahadir.trendyolcaseproject.R;
import com.trendyol.bahadir.trendyolcaseproject.enums.SortType;
import com.trendyol.bahadir.trendyolcaseproject.services.PhotoService;
import com.trendyol.bahadir.trendyolcaseproject.services.response.RecentPhotosResponse;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.Photo;
import com.trendyol.bahadir.trendyolcaseproject.ui.adapters.PhotoListAdapter;
import com.trendyol.bahadir.trendyolcaseproject.utils.ConnectivityUtil;
import com.trendyol.bahadir.trendyolcaseproject.utils.DialogUtil;
import com.trendyol.bahadir.trendyolcaseproject.utils.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class PhotoListFragment extends BaseFragment {

    @BindView(R.id.recycler_view_photos)
    protected RecyclerView recyclerViewPhotos;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    PhotoListAdapter photoListAdapter;
    private ArrayList<Photo> photos = new ArrayList<>();

    private SortType selectedSortType = SortType.RECENTLY_ADDED;

    public static PhotoListFragment newInstance() {
        return new PhotoListFragment();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_photo_list;
    }

    @Override
    protected void init() {
        if(ConnectivityUtil.isConnected(getActivity())){
            setupViews();
            fetchPhotos(1, SortType.RECENTLY_ADDED);
        }else{
            DialogUtil.showConnectionDialog(getActivity(),"Bağlantı Sorunu","Lütfen internet bağlantınızı kontrol ediniz.");
        }

    }

    private void setupViews() {
        toolbar.setTitle("Photo List");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        setHasOptionsMenu(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewPhotos.setHasFixedSize(true);
        recyclerViewPhotos.setLayoutManager(linearLayoutManager);
        photoListAdapter = new PhotoListAdapter(getActivity(), photos);
        recyclerViewPhotos.setAdapter(photoListAdapter);
        recyclerViewPhotos.addOnScrollListener(
                new EndlessRecyclerOnScrollListener(linearLayoutManager) {
                    @Override
                    public void onLoadMore(int current_page) {
                        fetchPhotos(current_page, selectedSortType);
                    }
                });
    }

    private void fetchPhotos(int current_page, SortType sortType) {
        switch (sortType) {
            case RECENTLY_ADDED:
                fetchRecentlyAddedPhotos(current_page);
                break;
            case TRENDYOL:
                fetchTrendyolPhotos(current_page);
                break;
            default:
                fetchRecentlyAddedPhotos(current_page);
        }
    }

    private void fetchRecentlyAddedPhotos(int current_page) {
        DialogUtil.showProgressDialog(getActivity());
        PhotoService.getRecentlyAddedPhotos(current_page, 20, new Callback<RecentPhotosResponse>() {
            @Override
            public void success(RecentPhotosResponse recentPhotosResponse, Response response) {
                Log.e("Success", "Success");
                DialogUtil.hideProgressDialog();
                if (recentPhotosResponse.getPhotos().getPhotos() != null) {
                    photos.addAll(recentPhotosResponse.getPhotos().getPhotos());
                    photoListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                DialogUtil.hideProgressDialog();
                DialogUtil.showDialog(getActivity(), "Hata", "Hata oluştu.");
            }
        });
    }


    private void fetchTrendyolPhotos(int current_page) {
        DialogUtil.showProgressDialog(getActivity());
        PhotoService.getTrendyolPhotos(current_page, new Callback<RecentPhotosResponse>() {
            @Override
            public void success(RecentPhotosResponse recentPhotosResponse, Response response) {
                Log.e("Success", "Success");
                DialogUtil.hideProgressDialog();
                photos.addAll(recentPhotosResponse.getPhotos().getPhotos());
                photoListAdapter.notifyDataSetChanged();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("failure", "failure");
                DialogUtil.hideProgressDialog();
                DialogUtil.showDialog(getActivity(), "Hata", "Hata oluştu.");
            }
        });
    }

    private void resetAdapter() {
        photos.clear();
        photoListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        resetAdapter();
        switch (item.getItemId()) {
            case R.id.recently_added:
                selectedSortType = SortType.RECENTLY_ADDED;
                fetchRecentlyAddedPhotos(1);
                return false;
            case R.id.trendyol:
                selectedSortType = SortType.TRENDYOL;
                fetchTrendyolPhotos(1);
                return false;
        }
        return false;
    }

    @Override
    protected void onSaveState(Bundle bundle) {

    }

    @Override
    protected void onRestoreState(Bundle bundle) {

    }

    @Override
    protected void onRestore() {

    }

}
