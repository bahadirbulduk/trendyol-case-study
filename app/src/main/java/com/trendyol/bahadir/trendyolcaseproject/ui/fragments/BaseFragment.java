package com.trendyol.bahadir.trendyolcaseproject.ui.fragments;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trendyol.bahadir.trendyolcaseproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseFragment extends Fragment {

  protected static final String PARAM_BUNDLE = "PARAM_BUNDLE";
  protected int fragmentId;

  private Bundle savedState;

  @Nullable
  @BindView(R.id.toolbar)
  protected Toolbar toolbar;

  public BaseFragment() {
    fragmentId = (int) System.nanoTime();
    setArguments(new Bundle());
  }

  @Override
  public View onCreateView(LayoutInflater inflater,
                           ViewGroup container, Bundle bundle) {
    View view = null;
    if (getLayoutResId() > 0) {
      view = inflater.inflate(getLayoutResId(), container, false);
      ButterKnife.bind(this, view);
    }
    setHasOptionsMenu(isHasOptionsMenu());
    return view;
  }

  private void initToolbar() {
    if (toolbar != null) {
      setSupportActionBar(toolbar);
      if (getTitleResId() != 0) {
        setTitle(getTitleResId());
      } else {
        setTitle("");
      }
    }
  }

  protected void setDisplayHomeAsUpEnabled(boolean isEnabled) {
    FragmentActivity activity = getActivity();
    if (activity instanceof AppCompatActivity) {
      ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
      if (actionBar != null) {
        actionBar.setDisplayHomeAsUpEnabled(isEnabled);
        actionBar.setHomeButtonEnabled(isEnabled);
      }
    }
  }

  @LayoutRes
  protected abstract int getLayoutResId();

  @StringRes
  protected int getTitleResId() {
    return 0;
  }

  @Override
  public void onActivityCreated(Bundle bundle) {
    super.onActivityCreated(bundle);
    initToolbar();
    if (!restoreStateFromArguments()) {
      init();
    } else {
      onRestore();
    }
  }

  protected abstract void onRestore();

  protected abstract void init();

  protected abstract void onSaveState(Bundle bundle);

  protected abstract void onRestoreState(Bundle bundle);

  @Override
  public void onSaveInstanceState(Bundle bundle) {
    saveStateToArguments();
    super.onSaveInstanceState(bundle);
  }

  @Override
  public void onDestroyView() {
    saveStateToArguments();
    super.onDestroyView();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onStart() {
    super.onStart();
  }

  @Override
  public void onStop() {
    super.onStop();
  }

  private void saveStateToArguments() {
    if (getView() != null) {
      savedState = saveState();
    }
    if (savedState != null) {
      Bundle bundle = getArguments();
      if (bundle != null) {
        bundle.putBundle(PARAM_BUNDLE, savedState);
      }
    }
  }

  private boolean restoreStateFromArguments() {
    Bundle bundle = getArguments();
    if (bundle == null || bundle.keySet().isEmpty()) {
      return false;
    }
    savedState = bundle.getBundle(PARAM_BUNDLE);
    if (savedState == null || savedState.keySet().isEmpty()) {
      return false;
    }
    restoreState();
    return true;
  }

  private void restoreState() {
    if (savedState != null) {
      onRestoreState(savedState);
    }
  }

  private Bundle saveState() {
    Bundle state = new Bundle();
    onSaveState(state);
    return state;
  }

  public int getFragmentId() {
    return fragmentId;
  }

  protected void finish() {
    if (getActivity() != null) {
      getActivity().finish();
    }
  }

  protected void setSupportActionBar(Toolbar toolbar) {
    FragmentActivity activity = getActivity();
    if (activity instanceof AppCompatActivity) {
      ((AppCompatActivity) activity).setSupportActionBar(toolbar);
    }
  }

  protected void setTitle(@StringRes int resId) {
    setTitle(getContext().getString(resId));
  }

  protected void setTitle(String title) {
    FragmentActivity activity = getActivity();
    if (activity instanceof AppCompatActivity) {
      ActionBar actionBar = ((AppCompatActivity) activity).getSupportActionBar();
      if (actionBar != null) {
        actionBar.setTitle(title);
      }
    }
  }

  public boolean isHasOptionsMenu() {
    return false;
  }

  public boolean onBackPressed() {
    return false;
  }

}