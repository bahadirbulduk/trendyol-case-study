package com.trendyol.bahadir.trendyolcaseproject.ui.holders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.BundleCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trendyol.bahadir.trendyolcaseproject.R;
import com.trendyol.bahadir.trendyolcaseproject.services.UserInfoService;
import com.trendyol.bahadir.trendyolcaseproject.services.response.UserInfoResponse;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.Person;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.Photo;
import com.trendyol.bahadir.trendyolcaseproject.ui.activities.CommentActivity;
import com.trendyol.bahadir.trendyolcaseproject.ui.activities.FullScreenActivity;
import com.trendyol.bahadir.trendyolcaseproject.ui.customviews.CircleImageView;
import com.trendyol.bahadir.trendyolcaseproject.ui.customviews.SquareImageView;
import com.trendyol.bahadir.trendyolcaseproject.utils.DialogUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class PhotoViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.card_view)
    protected CardView cardView;
    @BindView(R.id.image)
    protected SquareImageView image;
    @BindView(R.id.user_image)
    protected CircleImageView userImage;
    @BindView(R.id.user_name)
    protected TextView userName;
    @BindView(R.id.image_title)
    protected TextView imageTitle;

    private Context context;

    public PhotoViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bind(final Context context, final Photo photo) {
        this.context = context;
        imageTitle.setText(photo.getTitle());
        final String imageUrl = "https://farm" + photo.getFarm() + ".staticflickr.com/" + photo.getServer() + "/" + photo.getId() + "_" + photo.getSecret() + "_z.jpg";
        Glide.with(context)
                .load(imageUrl)
                .crossFade()
                .centerCrop()
                .into(image);

        UserInfoService.getUserInfo(photo.getOwner(), new Callback<UserInfoResponse>() {
            @Override
            public void success(UserInfoResponse userInfoResponse, Response response) {
                userName.setText(userInfoResponse.getPerson().getPersonUserName().getUserName());
                Person person = userInfoResponse.getPerson();
                String userImageUrl;
                if (person.getIconserver().equals("0")) {
                    userImageUrl = "https://www.flickr.com/images/buddyicon.png";
                } else {
                    userImageUrl = "http://farm" + person.getIconfarm() + ".staticflickr.com/" + person.getIconserver() + "/buddyicons/" + person.getNsid() + ".jpg";
                }
                Glide.with(context)
                        .load(userImageUrl)
                        .crossFade()
                        .fitCenter()
                        .into(userImage);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, FullScreenActivity.class);
                Bundle b = new Bundle();
                b.putString("image_url", imageUrl);
                intent.putExtras(b);
                context.startActivity(intent);
            }
        });

        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CommentActivity.class);
                Bundle b = new Bundle();
                b.putString("image_id", photo.getId());
                intent.putExtras(b);
                context.startActivity(intent);
            }
        });
    }
}
