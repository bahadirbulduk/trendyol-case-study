package com.trendyol.bahadir.trendyolcaseproject.services;

import com.trendyol.bahadir.trendyolcaseproject.services.response.UserInfoResponse;

import retrofit.Callback;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class UserInfoService {

    public static void getUserInfo(String userId, Callback<UserInfoResponse> callback) {
        BaseService.getApi().getUserInfo(userId, callback);
    }
}
