package com.trendyol.bahadir.trendyolcaseproject.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trendyol.bahadir.trendyolcaseproject.R;
import com.trendyol.bahadir.trendyolcaseproject.services.response.RecentPhotosResponse;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.Photo;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.PhotoList;
import com.trendyol.bahadir.trendyolcaseproject.ui.holders.PhotoViewHolder;

import java.util.ArrayList;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class PhotoListAdapter extends RecyclerView.Adapter<PhotoViewHolder> {

    private Context context;
    private ArrayList<Photo> photos;

    public PhotoListAdapter(Context context, ArrayList<Photo> photos) {
        this.context = context;
        this.photos = photos;
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_photo_item, parent, false);
        return new PhotoViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        holder.bind(context, photos.get(position));
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}
