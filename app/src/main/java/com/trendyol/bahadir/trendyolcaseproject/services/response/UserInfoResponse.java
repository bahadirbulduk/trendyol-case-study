package com.trendyol.bahadir.trendyolcaseproject.services.response;

import com.google.gson.annotations.SerializedName;
import com.trendyol.bahadir.trendyolcaseproject.services.response.model.Person;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class UserInfoResponse {
    @SerializedName("person")
    private Person person;
    @SerializedName("stat")
    private String stat;

    public Person getPerson() {
        return person;
    }

    public String getStat() {
        return stat;
    }
}
