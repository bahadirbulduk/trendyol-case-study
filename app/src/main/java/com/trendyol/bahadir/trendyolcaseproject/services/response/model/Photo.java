package com.trendyol.bahadir.trendyolcaseproject.services.response.model;

import com.google.gson.annotations.SerializedName;

public class Photo {

    @SerializedName("id")
    private String id;
    @SerializedName("owner")
    private String owner;
    @SerializedName("secret")
    private String secret;
    @SerializedName("server")
    private String server;
    @SerializedName("farm")
    private String farm;
    @SerializedName("title")
    private String title;
    @SerializedName("ispublic")
    private String isPublic;
    @SerializedName("isfriend")
    private String isFriend;
    @SerializedName("isfamily")
    private String isFamily;

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public String getSecret() {
        return secret;
    }

    public String getServer() {
        return server;
    }

    public String getFarm() {
        return farm;
    }

    public String getTitle() {
        return title;
    }

    public String getIsPublic() {
        return isPublic;
    }

    public String getIsFriend() {
        return isFriend;
    }

    public String getIsFamily() {
        return isFamily;
    }
}
