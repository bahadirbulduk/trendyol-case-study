package com.trendyol.bahadir.trendyolcaseproject.services;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

public class ApiErrorHandler implements ErrorHandler {

  @Override
  public Throwable handleError(RetrofitError cause) {
    //here place your logic for all errors
    return cause;
  }
}