package com.trendyol.bahadir.trendyolcaseproject.services.response.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PhotoList {
    @SerializedName("page")
    private Integer page;
    @SerializedName("pages")
    private Integer pages;
    @SerializedName("perpage")
    private Integer perPage;
    @SerializedName("total")
    private Integer total;
    @SerializedName("photo")
    private ArrayList<Photo> photos;

    public Integer getPage() {
        return page;
    }

    public Integer getPages() {
        return pages;
    }

    public Integer getPerPage() {
        return perPage;
    }

    public Integer getTotal() {
        return total;
    }

    public ArrayList<Photo> getPhotos() {
        return photos;
    }
}
