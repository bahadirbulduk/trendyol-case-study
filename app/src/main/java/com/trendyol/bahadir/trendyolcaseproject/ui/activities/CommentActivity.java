package com.trendyol.bahadir.trendyolcaseproject.ui.activities;

import com.trendyol.bahadir.trendyolcaseproject.R;
import com.trendyol.bahadir.trendyolcaseproject.ui.fragments.BaseFragment;
import com.trendyol.bahadir.trendyolcaseproject.ui.fragments.CommentFragment;

/**
 * Created by Bahadir on 25.02.2017.
 */

public class CommentActivity extends BaseActivity {

    @Override
    protected BaseFragment getFragment() {
        String imageId = getIntent().getExtras().getString("image_id");
        return CommentFragment.newInstance(imageId);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_base_template;
    }
}
