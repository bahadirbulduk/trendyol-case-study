package com.trendyol.bahadir.trendyolcaseproject.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;

@SuppressLint("StaticFieldLeak")
public final class DialogUtil {

    private static MaterialDialog progressDialog;
    private static MaterialDialog infoDialog;
    private static MaterialDialog forgetPasswordDialog;
    private static EditText email;

    private DialogUtil() {
    }

    public static void showProgressDialog(Context context) {
        showProgressDialog(context, "Lütfen bekleyiniz.");
    }

    public static void showProgressDialog(Context context, int resourceId) {
        showProgressDialog(context, context.getString(resourceId));
    }

    public static void showProgressDialog(Context context, String message) {
        if (progressDialog != null) {
            progressDialog.setContent(message);
            return;
        }
        progressDialog
                = new MaterialDialog.Builder(context)
                .content(message)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    public static void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }

    public static void showDialog(Context context, String title, String message) {
        new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .positiveText("Tamam")
                .cancelable(false)
                .show();
    }

    public static void showConnectionDialog(final Context context, String title, String
            message) {
        new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .cancelable(false)
                .show();
    }
}